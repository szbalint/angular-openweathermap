import {IOpenWeatherMapData} from '../interfaces/iopen-weather-map-data';
import {IWeather} from '../interfaces/iweather';


export class WeatherData {
  actualWeather: IWeather[];
  temperature: number;
  humidity: number;
  windSpeed: number;

  constructor(data: IOpenWeatherMapData) {
    this.actualWeather = data.weather;
    this.temperature = data.main.temp ;
    this.humidity = data.main.humidity;
    this.windSpeed = data.wind.speed;

  }
}
