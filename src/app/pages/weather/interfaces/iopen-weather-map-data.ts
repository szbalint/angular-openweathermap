import {ICoord} from './icoord';
import {IWeather} from './iweather';
import {IMain} from './imain';
import {IWind} from './iwind';
import {ICloud} from './icloud';
import {ISys} from './isys';
import {IRain} from './irain';

export interface IOpenWeatherMapData {
  coord: ICoord;
  weather: IWeather[];
  base: string;
  main: IMain;
  wind: IWind;
  clouds: ICloud;
  rain: IRain;
  dt: number;
  sys: ISys;
  id: number;
  name: string;
  cod: number;
}

