import {Injectable} from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class CityService {
  private readonly _cities: string[];

  constructor() {
    this._cities = [
      'Veszprém',
      'Várpalota',
      'Budapest',
      'Debrecen',
      'Pécs',
      'Szombathely',
      'Székesfehérvár',
      'Komárom',
      'Érd',
      'Miskolc',
      'Szeged'

    ];
  }

  getCities(): string[] {
    return this._cities;
  }
}
