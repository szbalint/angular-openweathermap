import {Injectable} from '@angular/core';


import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {WeatherData} from '../models/weather-data';
import {IOpenWeatherMapData} from '../interfaces/iopen-weather-map-data';
import {RequestService} from '../../../services/request.service';


@Injectable({
  providedIn: 'root'
})
export class OpenWeatherMapService {
  private TOKEN = '9fced472758819a4d65fb8bcf9d0f772';
  // private  API_URL = '/api/open-weather-map';
  private API_URL = 'http://api.openweathermap.org/data/2.5/weather';
  private UNIT = 'metric';
  private LANG = 'hu';

  constructor(private  _requestService: RequestService) {
  }

  getCurrentWeatherByCity(city: string): Observable<WeatherData> {
    return this._requestService.get<IOpenWeatherMapData>(this.generateURL(city)).pipe(map(
      (response: IOpenWeatherMapData) => {
        return new WeatherData(response);
      }
    ));
  }

  generateURL(city: string): string {
    return this.API_URL + '?APPID='
      + this.TOKEN + '&q=' + city + '&units=' + this.UNIT + '&lang=' + this.LANG;
  }
}
