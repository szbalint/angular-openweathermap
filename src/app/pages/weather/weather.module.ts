import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WeatherComponent} from './components/weather/weather.component';
import {WeatherTableComponent} from './components/weather-table/weather-table.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {WeatherRoutingModule} from './weather-routing.module';

@NgModule({
  declarations: [
    WeatherComponent,
    WeatherTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    WeatherRoutingModule
  ]
})
export class WeatherModule {
}
