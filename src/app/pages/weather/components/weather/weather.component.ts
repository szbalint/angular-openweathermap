import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {OpenWeatherMapService} from '../../services/open-weather-map.service';
import {CityService} from '../../services/city.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {WeatherData} from '../../models/weather-data';


@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {
  currentWeatherData: WeatherData | null = null;
  msg: string = '';
  cityForm: FormGroup;
  cities: string[] = [];

  constructor(private _openWeatherDataService: OpenWeatherMapService,
              cityService: CityService,
              private _fb: FormBuilder
  ) {
    this.cities = cityService.getCities();
    this.cityForm = this.createFormGroup();
  }

  ngOnInit() {
  }

  submit(): void {
    this.getWeatherData(this.cityForm.value.city).subscribe((response: WeatherData) => {
        this.currentWeatherData = response;
      }, error => {
        this.msg = 'Hiba történt :' + error;
      }
    );
  }

  getWeatherData(city: string): Observable<WeatherData> {
    return this._openWeatherDataService.getCurrentWeatherByCity(city);
  }

  createFormGroup(): FormGroup {
    return this._fb.group({
      city: ['', [Validators.required, Validators.minLength(1)]]
    });
  }
}
