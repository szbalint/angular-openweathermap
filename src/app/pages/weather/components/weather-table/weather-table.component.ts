import {Component, Input, OnInit} from '@angular/core';
import {WeatherData} from '../../models/weather-data';


@Component({
  selector: 'app-weather-table',
  templateUrl: './weather-table.component.html',
  styleUrls: ['./weather-table.component.css']
})
export class WeatherTableComponent implements OnInit {
  private ICON_URL = 'http://openweathermap.org/img/w/';
  private ICON_EXT = '.png';

  @Input()
  currentWeatherData: WeatherData | null;

  constructor() {
  }

  ngOnInit() {
  }

  getIconUrl(icon: string): string {
    return this.ICON_URL + icon + this.ICON_EXT;
  }

}
