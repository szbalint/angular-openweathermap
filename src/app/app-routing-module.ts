import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WeatherModule} from './pages/weather/weather.module';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'weather-module',
  },
  {
    path: 'weather-module',
    loadChildren: () => WeatherModule
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
