import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class RequestService {

  constructor(private _http: HttpClient) {
  }

  get<T>(url: string, headers?: HttpHeaders | null): Observable<T> {
    const expandedHeaders = this.prepareHeader(headers);
    return this._http.get<T>(url, expandedHeaders);
  }

  post<T>(url: string, body: string, headers?: HttpHeaders | null): Observable<T> {
    const expandedHeaders = this.prepareHeader(headers);
    return this._http.post<T>(url, body, expandedHeaders);
  }

  put<T>(url: string, body: any, headers?: HttpHeaders | null): Observable<T> {
    const expandedHeaders = this.prepareHeader(headers);
    return this._http.put<T>(url, body, expandedHeaders);
  }

  patch<T>(url: string, body: string, headers?: HttpHeaders | null): Observable<T> {
    const expandedHeaders = this.prepareHeader(headers);
    return this._http.patch<T>(url, body, expandedHeaders);
  }

  delete<T>(url: string, headers?: HttpHeaders | null): Observable<T> {
    const expandedHeaders = this.prepareHeader(headers);
    return this._http.delete<T>(url, expandedHeaders);
  }


  private prepareHeader(headers: HttpHeaders | null): object {
    headers = headers ? headers : new HttpHeaders();
    headers = headers.append('Content-Type', 'text/plain');
    headers = headers.append('Accept', 'application/json');
    return {
      headers: headers,
      observe: 'body'
    };
  }
}
